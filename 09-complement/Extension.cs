﻿namespace _09_complement
{
    // Méthodes d'extension pour ajouter des fonctionnalités à des classes ou des structures existantes
    // Elle doit être définie dans une classe static
    internal static class Extension
    {
        // Une méthode d'extension doit être static et le premier paramètre doit être : this typeEtendu nomParametre
        // Ici, on ajoute une méthode Capitalize à la classe de la BCLk string
        public static string Capitalize(this string str)
        {
            return str.Substring(0, 1).ToUpper() + str.Substring(1).ToLower();
        }
    }
}
