﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_complement
{
    internal class Box<T> //where T : Animal
    {
        public T A { get; set; }

        public T B { get; set; }

        public Box(T a, T b)
        {
            A = a;
            B = b;
        }

        public override string ToString()
        {
            return $"{A} {B}";
        }
    }
}
