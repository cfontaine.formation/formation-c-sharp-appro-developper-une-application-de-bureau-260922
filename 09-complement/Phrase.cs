﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _09_complement
{
    internal class Phrase
    {
        List<string> _mots = new List<string>();

        public Phrase(string phraseStr)
        {
            string[] tmpStr = phraseStr.Split();
            foreach (var s in tmpStr)
            {

                _mots.Add(s);
            }
        }

        // Indexeur
        public string this[int index]
        {
            get
            {
                return _mots[index];
            }
            set
            {
                _mots[index] = value;
            }
        }

        // On peut déclarer plusieurs indexeurs, chacun avec des paramètres de différents types
        public int this[string mot]
        {
            get
            {
                int index = _mots.IndexOf(mot);
                if (index >= 0)
                {
                    return index;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var s in _mots)
            {
                sb.Append(s);
                sb.Append(' ');
            }
            return sb.ToString();
        }
    }
}
