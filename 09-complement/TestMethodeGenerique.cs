﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_complement
{
    internal class TestMethodeGenerique
    {
        public static void MethodeGenerique<T>(T a)
        {
            Console.WriteLine(a);
        }

        public static void MethodeGenerique2<T,V>(T a,V b)
        {
            Console.WriteLine(a);
            Console.WriteLine(b);
        }
    }
}
