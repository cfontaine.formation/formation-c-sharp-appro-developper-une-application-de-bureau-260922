﻿using System;
using System.Reflection;

namespace _09_complement
{
    internal class Program
    {

        static void Main(string[] args)
        {
            #region Type nullable
            // int i = null; // type simple
            int? i;
            i = null;
            // HasValue ->  permet de tester s'il y a une valeur diférent de null
            //              équivalent à  i != null
            Console.WriteLine(i.HasValue); // false
            // Value ->     permet d'accéder à la valeur, si null -> exception
            //              équivalant à un cast
            if (i.HasValue)
            {
                Console.WriteLine(i.Value);
            }
            double? d = 1.2;    // Conversion implicite(double vers nullable)
            Console.WriteLine(d.HasValue); // true
            if (d.HasValue)
            {
                Console.WriteLine(d.Value); // 1.2
                double res = (double)d;
                Console.WriteLine(res);
            }
            #endregion

            // Méthode locale
            AfficherSquare(2);
            AfficherSquareCapture(2);

            // Méthode d'extension (=> voir Extension.cs)
            // ajouter des fonctionnalités à des classes existantes
            string str = "hello WORLD";
            Console.WriteLine(str.Capitalize()); // Hello world

            #region Indexeur
            // Les indexeurs permettent l’utilisation de propriétés indexées, qui sont référencées à l’aide d’un ou plusieurs arguments
            Console.WriteLine(str[0]);

            Phrase ph = new Phrase("Il fait beau aujourd'hui");
            Console.WriteLine(ph);
            Console.WriteLine(ph[0]); // Il
            ph[3] = "demain";
            Console.WriteLine(ph);

            Console.WriteLine(ph["fait"]); // 1
            // Console.WriteLine(ph["aaaa"]); // => IndexOutOfRangeException
            #endregion

            #region Type Générique

            // Sur les classes
            Box<string> b1 = new Box<string>("hello", "world");
            Console.WriteLine(b1);

            Box<int> b2 = new Box<int>(1, 3);
            Console.WriteLine(b2);

            // Sur le méthodes
            TestMethodeGenerique.MethodeGenerique<int>(12);
            TestMethodeGenerique.MethodeGenerique<string>("hello");

            // Le type déterminé par le compilateur en fonction du type du paramètre
            TestMethodeGenerique.MethodeGenerique(12);
            TestMethodeGenerique.MethodeGenerique("hello");
            TestMethodeGenerique.MethodeGenerique(b1);

            TestMethodeGenerique.MethodeGenerique2("hello", 1);
            #endregion

            #region Reflexion / introspection
            Type type = typeof(string);
            Console.WriteLine(type.FullName);
            FieldInfo[] fields = type.GetFields();
            foreach (var f in fields)
            {
                Console.WriteLine(f);
            }

            PropertyInfo[] pr = type.GetProperties();
            foreach (var p in pr)
            {
                Console.WriteLine(p);
            }

            MethodInfo[] mi = type.GetMethods();
            for (int k = 0; k < mi.Length; k++)
            {
                Console.WriteLine($"{k} {mi[k]}");
            }


            ConstructorInfo[] construteur = type.GetConstructors();
            for (int k = 0; k < construteur.Length; k++)
            {
                Console.WriteLine($"{k} {construteur[k]}");
            }

            object strObject = construteur[6].Invoke(new object[] { new char[] { 'a', 'z', 'e', 'r' } });
            MethodInfo methInfo = type.GetMethod("ToUpper", Type.EmptyTypes);
            object obj = methInfo.Invoke(strObject, null); // ToUpper
            string resStr = obj as string;
            Console.WriteLine(resStr);
            #endregion

            Console.ReadKey();
        }

        #region Méthodes locales
        // Méthode locale à partir c# 7.0
        static void AfficherSquare(double v)
        {
            Console.WriteLine(Square(v));
            // Une méthode locale n'est visible que par la méthode englobante
            double Square(double v1) => v1 * v1;
        }

        static void AfficherSquareCapture(double v)
        {
            // Capture
            // Une méthode locale peut accéder aux variables locales et aux paramètres de la méthode englobante
            //double w = v;
            Console.WriteLine(Square());
            //  double Square() => w * w
            double Square() => v * v;
        }
        #endregion
    }
}
