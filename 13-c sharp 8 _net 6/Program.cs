﻿
int numJour = int.Parse(Console.ReadLine());
string jour = numJour switch
{
    1 => "Lundi",
    2 => "Mardi",
    7 => "Dimanche",
    _ => "Un autre jour" // _ équivalant à 'default'
};
Console.WriteLine(jour);
char[] v = new char[] { 'a', 'e', 'i', 'o', 'u' };
Console.WriteLine(v[^1]);
Console.WriteLine("_______________");
char[] v2 = v[1..3];
foreach (char c in v2)
{
    Console.WriteLine(c);
}


