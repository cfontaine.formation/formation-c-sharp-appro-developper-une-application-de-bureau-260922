﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _08_exemplesuite
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonAjouter_Click(object sender, EventArgs e)
        {
           for(int i= listBoxArticle.SelectedIndices.Count - 1; i >= 0; i--)
            {
                int indice=listBoxArticle.SelectedIndices[i];
                listBoxCommande.Items.Add(listBoxArticle.Items[indice]);
                listBoxArticle.Items.RemoveAt(indice);
            }
        }

        private void buttonCombo_Click(object sender, EventArgs e)
        {
            label1.Text = comboBox1.Text;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            label2.Text = cb.Checked ? "Remise de 10%" : "";
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            label3.Text = rb.Text;
            switch (rb.Text)
            {
                case "rouge":
                    rb.Parent.BackColor=Color.Red;
                    break;
                case "vert":
                    rb.Parent.BackColor = Color.Green;
                    break;
                case "bleu":
                    rb.Parent.BackColor = Color.Blue;
                    break;
            }
        }
    }
}
