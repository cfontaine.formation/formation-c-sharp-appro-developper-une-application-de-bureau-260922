﻿using _03_LibBdd;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace _05_annuaire
{
    public partial class Form1 : Form
    {
        ContactDao dao = new ContactDao();
        public Form1()
        {
            InitializeComponent();
            ContactDao.ChaineConnection = ConfigurationManager.ConnectionStrings["chmysql"].ConnectionString;
            InitView(dao.FindAll());
        }

        private void buttonQuitter_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Voulez vous quitter?", "Annuaire", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                Close();
                // ou
                //Application.Exit();
            }
        }

        private void InitView(List<Contact> contacts)
        {
            listView1.Items.Clear();
            foreach (var c in contacts)
            {
                ListViewItem item = new ListViewItem(c.Id.ToString(), 0);
                item.SubItems.Add(c.Prenom);
                item.SubItems.Add(c.Nom);
                item.SubItems.Add(c.DateNaissance.ToShortDateString());
                item.SubItems.Add(c.Email);
                listView1.Items.Add(item);
            }
        }

        private void buttonAjouter_Click(object sender, EventArgs e)
        {
            FormAjoutModif formAjout = new FormAjoutModif();
            if (formAjout.ShowDialog() == DialogResult.OK)
            {
                dao.SaveOrUpdate(formAjout.ContactForm);
                InitView(dao.FindAll());
            }
            formAjout.Close();
            formAjout.Dispose();
        }

        private void buttonModifier_Click(object sender, EventArgs e)
        {
            var selected = listView1.SelectedItems;
            if (selected.Count != 0)
            {
                long id = Convert.ToInt64(selected[0].SubItems[0].Text);
                FormAjoutModif formAjout = new FormAjoutModif(dao.FindById(id));
                if (formAjout.ShowDialog() == DialogResult.OK)
                {
                    dao.SaveOrUpdate(formAjout.ContactForm);
                    InitView(dao.FindAll());
                }
                formAjout.Close();
                formAjout.Dispose();
            }
        }

        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            var selected = listView1.SelectedItems;
            if (selected.Count != 0)
            {
                long id = Convert.ToInt64(selected[0].SubItems[0].Text);
                if (MessageBox.Show("Voulez suppimer ce contact?", "Annuaire", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    dao.Remove(id);
                    InitView(dao.FindAll());
                }
            }
        }
    }
}
