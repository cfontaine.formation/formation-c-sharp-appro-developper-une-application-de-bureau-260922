﻿using _03_LibBdd;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace _05_annuaire
{
    public partial class FormAjoutModif : Form
    {
        public Contact ContactForm { get; private set; }
        public FormAjoutModif()
        {
            InitializeComponent();
            Text = "Ajouter Contact";
        }

        public FormAjoutModif(Contact contactForm) : this()
        {
            Text = "Modifier Contact";
            ContactForm = contactForm;
            textBoxPrenom.Text = contactForm.Prenom;
            textBoxNom.Text = contactForm.Nom;
            dateTimePicker1.Value = contactForm.DateNaissance;
            textBoxEmail.Text = contactForm.Email;
            ContactForm.Id = contactForm.Id;
        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            string msgError = CheckContact();
            if (msgError != null)
            {
                MessageBox.Show(msgError, "Annuaire", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (ContactForm == null)
                {
                    ContactForm = new Contact(textBoxPrenom.Text, textBoxNom.Text, dateTimePicker1.Value, textBoxEmail.Text);
                }
                else
                {
                    ContactForm.Prenom = textBoxPrenom.Text;
                    ContactForm.Nom = textBoxNom.Text;
                    ContactForm.DateNaissance = dateTimePicker1.Value;
                    ContactForm.Email = textBoxEmail.Text;
                }
                DialogResult = DialogResult.OK;
            }
        }

        private string CheckContact()
        {
            if (textBoxPrenom.Text == "")
            {
                return "Le champs prénom n'est pas saisie";
            }
            else if (textBoxNom.Text == "")
            {
                return "Le champs nom n'est pas saisie";
            }
            else if (dateTimePicker1.Value > DateTime.Now)
            {
                return "La date de naissance n'est pas valide";
            }
            else if (textBoxEmail.Text == "")
            {
                return "L'email  n'est pas saisie";
            }
            else if (!Regex.IsMatch(textBoxEmail.Text, "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"))
            {
                return "L'email n'est pas valide";
            }
            return null;
        }
    }
}
