﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_delegation
{
    internal class CompteBancaire
    {
        double solde = 100;

        //public delegate void SoldeNegatif();

        //public event SoldeNegatif OnSoldeNegatif;

        // EventHandler
        //  public event EventHandler OnSoldeNegatif;

        //EventHandler<TEventArgs>
        public event EventHandler<SoldeNegatifArgs> OnSoldeNegatif;
        public CompteBancaire(double solde)
        {
            this.solde = solde;
        }

        public void Debiter(double montant)
        {
            solde -= montant;
            if (solde < 0)
            {
               // OnSoldeNegatif()
              //  OnSoldeNegatif(this, EventArgs.Empty);
              OnSoldeNegatif(this,new SoldeNegatifArgs(solde));
            }
        }
    }
}
