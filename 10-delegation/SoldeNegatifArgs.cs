﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_delegation
{
    internal class SoldeNegatifArgs : EventArgs
    {
        public double Solde { get; set; }

        public SoldeNegatifArgs(double solde)
        {
            Solde = solde;
        }
    }
}
