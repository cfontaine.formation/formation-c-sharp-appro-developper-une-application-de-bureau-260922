﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_delegation
{
    internal class Program
    {
        // Protoype d'une méthode
        public delegate int Operation(int a, int b);

        public delegate bool Comparaison(int v1, int v2);

        // C# 1.0
        //-----------------------
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Multiplier(int a, int b)
        {
            return a * b;
        }
        //------------------------

        static void Main(string[] args)
        {
            // C# 1
            Operation add = new Operation(Ajouter);
            Console.WriteLine(Calculer(1, 2, add));
            Console.WriteLine(Calculer(1, 2, new Operation(Multiplier)));

            // C# 2
            Operation add2 = delegate(int i1,int i2) { return i1 + i2; };

            Console.WriteLine(Calculer(1, 2, add2));
            Console.WriteLine(Calculer(1, 2, delegate (int i1, int i2) { return i1 * i2; }));

            // C# 3
            Console.WriteLine(Calculer(1, 2, (o1, o2) => o1 + o2));
            Console.WriteLine(Calculer(1, 2, (o1, o2) => o1 * o2));

            int[] tab = { 5, 8, 1, 6, -3, 10, -1 };
            SortTab(tab,(i1,i2) => i1<i2);

            foreach(int e in tab)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine(CalculerF(3, 4, (i1, i2) => i1 - i2));

            double d = 42.0;
            Func<double, double> multi = x => x * d;
            Console.WriteLine(multi(20));

            // événement
            CompteBancaire cb = new CompteBancaire(200.0);
            cb.OnSoldeNegatif += SmsSoldeNegatif;
            cb.OnSoldeNegatif += EmailSoldeNegatif;
            double valeur = Convert.ToDouble(Console.ReadLine());
            cb.Debiter(valeur);
            Console.WriteLine("------------");
            Console.ReadKey();
        }

        public static int Calculer(int v1, int v2, Operation op)
        {
            return op(v1, v2);
        }
        public static int CalculerF(int v1, int v2, Func<int,int,int> op)
        {
            return op(v1, v2);
        }

        static void SortTab(int[] tab, Comparaison cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }
        // Evenement
        //static void SmsSoldeNegatif()
        //{
        //    Console.WriteLine("SMS solde négatif");
        //}
        //static void EmailSoldeNegatif()
        //{
        //    Console.WriteLine("Email solde négatif");
        //}

        //static void SmsSoldeNegatif(object sender, EventArgs e)
        //{
        //    Console.WriteLine("SMS solde négatif");
        //}
        //static void EmailSoldeNegatif(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Email solde négatif");
        //}

        static void SmsSoldeNegatif(object sender, SoldeNegatifArgs e)
        {
            Console.WriteLine($"SMS solde négatif ={e.Solde}");
        }
        static void EmailSoldeNegatif(object sender, SoldeNegatifArgs e)
        {
            Console.WriteLine($"Email solde négatif = {e.Solde}");
        }

    }
}
