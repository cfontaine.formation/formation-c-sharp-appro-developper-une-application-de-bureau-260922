﻿using _03_LibBdd;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace _04_Bdd
{
    internal class Program
    {
        // Pour ajouter au projet la bibliothèque
        // Cliquer droit sur le projet -> Ajouter -> référence -> choisir 03-LibBdd
        static void Main(string[] args)
        {
            Contact c = new Contact("John", "Doe", new DateTime(1994, 09, 28), "jd@dawan.com");
            TestBdd(c);
            TestDao(c);
            Console.ReadKey();
        }

        static void TestBdd(Contact contact)
        {
            // La chaine de connection contient toutes les informations pour la connection à la base de donnée
            // Server-> adresse du serveur de bdd
            // Port port de serveur de bdd
            // Database -> nom de la base de donnée
            // Uid -> utilisateur de la base de donnée
            // Pwd -> mot de passe de la bdd

            // Chaine de connection MySQL en local pour la base de donnée
            string chaineConnection = "Server=localhost;Port=3306;Database=formation;Uid=root;Pwd=dawan;SslMode=none";

            // Création de la connexion à la base de donnée SqlConnection
            MySqlConnection cnx = new MySqlConnection(chaineConnection);

            // Ouverture de la connexion à la base de donnée
            cnx.Open();

            // MySqlCommand -> utilisé pour exécuter une requête sur la base de données
            string requete = "INSERT INTO contacts(prenom,nom,date_naissance,email) VALUES(@prenom,@nom,@date_naissance,@email)";
            MySqlCommand cmd = new MySqlCommand(requete, cnx);

            // Remplacement des paramètres dans la requête (@...) par les valeurs
            cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
            cmd.Parameters.AddWithValue("@nom", contact.Nom);
            cmd.Parameters.AddWithValue("@date_naissance", contact.DateNaissance);
            cmd.Parameters.AddWithValue("@email", contact.Email);
            cmd.ExecuteNonQuery();      // execution de la requete pour INSERT,UPDATE,DELETE

            requete = "SELECT id,prenom,nom,date_naissance,email FROM contacts";
            cmd = new MySqlCommand(requete, cnx);

            // MySqlDataReader -> utilisé pour récupérer des données de la base de données.
            MySqlDataReader r = cmd.ExecuteReader();    // execution de la requete pour SELECT
            while (r.Read())    // Read() permet de passer à la prochaine "ligne" retourne false quand il n'y a plus de resultat
            {
                Console.WriteLine($"{r.GetInt64(0)},{r.GetString(1)},{r.GetString(2)},{r.GetDateTime(3)},{r.GetString(4)}");
            }

            // Fermeture de la connection
            cnx.Close();
            cnx.Dispose();
        }

        static void TestDao(Contact c)
        {
            // Récupération de la chaine de connection dans le fichier App.config du projet élément<connectionStrings>
            ContactDao.ChaineConnection = ConfigurationManager.ConnectionStrings["chmysql"].ConnectionString;

            ContactDao dao = new ContactDao();
            Console.WriteLine(c);       // id=0 => l'objet n'est pas persisté en base de donnée
            dao.SaveOrUpdate(c, false); // persister l'objet dans la bdd
            Console.WriteLine(c);       // id a été généré par la bdd

            // Récupéré un contact dans la bdd à partir de son id
            Contact o = dao.FindById(c.Id, false);
            Console.WriteLine(o);

            // Modification 
            c.Email = "other@dawan.fr";
            dao.SaveOrUpdate(c, false);

            // Affichage de tous les objets contact dans la bdd
            List<Contact> lst = dao.FindAll(false);
            foreach (var e in lst)
            {
                Console.WriteLine(e);
            }

            // supprimer un contact
            dao.Remove(c, false);

            lst = dao.FindAll();
            foreach (var e in lst)
            {
                Console.WriteLine(e);
            }

        }
    }
}
