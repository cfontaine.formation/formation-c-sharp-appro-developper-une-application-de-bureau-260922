﻿using System;
using System.Diagnostics;
using System.Threading;

namespace _11_Thread
{
    internal class Program
    {

        static int _sharedData;                 // Donnée partagée
        static object _locker = new object();   // Pour lock, il faut une référence

        static void Main(string[] args)
        {
            #region Classe Thread
            // ThreadStart -> Délégué qui permet de créer Thread (Méthode sans paramètre)
            Thread tsp = new Thread(new ThreadStart(ThreadSansParam));    // Le constructeur de la classe Thread prend en paramètre un délégué
            tsp.Start();                                                  //  La Méthode Start permet de démmarrer le Thread

            // ParamterizedThreadStart -> Délégué qui permet de créer Thread (méthode qui a un paramètre de "type" object)
            Thread tap = new Thread(new ParameterizedThreadStart(ThreadParam));
            tap.Start(50);
            #endregion

            #region Propriété
            Thread t1 = new Thread(new ParameterizedThreadStart(ThreadParam));
            t1.Name = "T1";                         // La propriété Name permet de nommer le Thread
                                                    // t1.Priority = ThreadPriority.Lowest;    // La propriété Priority permet de modifier la prioritédu thread

            // Highest => le thread est le + prioritaire, AboveNormal, Normal (par défaut), BelowNormal, Lowest => le thread est le - prioritaire
            Console.WriteLine($"état du Thread à la création = {t1.ThreadState}");    // La propriétée ThreadState contient l'état du Thread, au départ => Unstarted
            t1.Start(50);
            Console.WriteLine($"état du Thread après L'éxécution de la méthode Start= {t1.ThreadState}"); // Runnable ou running s'il est executé
            //t1.IsBackground = true;

            #endregion

            // Thread Main
            for (int i = 0; i < 40; i++)
            {
                Console.WriteLine($"Main - {i}");
                Thread.Sleep(50);
            }

            #region Méthode Abort
            // Un thread démarre avec l'execution de la méthode Start et finit lorsque la méthode associé est finie 
            // On peut arréter les Thread aussi avec la méthode Arbort
            try
            {
                t1.Abort();
                Console.WriteLine($"état du Thread après l'arrét du Thread avec La méthode Arbort= {t1.ThreadState}");
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine($"Problème Abort {e.StackTrace}");
            }
            #endregion

            #region Thread join
            // Join 
            // bloque tous les threads appelants jusqu'à ce que ce thread se termine
            Thread t2 = new Thread(new ParameterizedThreadStart(ThreadParam));
            t2.Start();
            t2.Join(100);  // Attend que le thread t2 soit finit pour continuer

            // Thread Main
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine($"Main - {i}");
                Thread.Sleep(50);
            }
            #endregion

            #region Verrouiller un Thread
            Thread tlt = new Thread(new ThreadStart(ThreadLockTest));
            tlt.Name = "TLT";
            tlt.Start();
            ThreadLockTest();
            #endregion

            #region Timer
            Timer t = new Timer(TestTimer, "Message du timer",
            0,          //appel tout de suite
            1000);      //et toutes les secondes
            #endregion

            #region Processus
            Process p2 = Process.Start("Notepad.exe");
            #endregion
            Console.WriteLine("Fin de la méthode Main");

            Console.ReadKey();

        }

        static void ThreadSansParam()
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine($"{Thread.CurrentThread.Name}: {i}");
            }
        }

        static void ThreadParam(object time)
        {
            if (time is int t)
            {
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine($"{Thread.CurrentThread.Name}: {i}");
                    Thread.Sleep(t);
                }
            }
        }

        static void ThreadLockTest()
        {
            lock (_locker)
            {
                if (_sharedData == 0)
                {
                    ++_sharedData;
                }
                Console.WriteLine($"\tLock-{Thread.CurrentThread.Name}-{_sharedData}");
            }
        }

        static void TestTimer(object state)
        {
            Console.WriteLine(state);
        }


    }
}