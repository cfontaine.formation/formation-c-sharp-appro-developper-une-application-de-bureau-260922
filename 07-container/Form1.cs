﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace _06_container
{
    public partial class Form1 : Form
    {

        List<Button> lstBp = new List<Button>();
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonAjout_Click(object sender, EventArgs e)
        {
            Button bd = new Button();
            bd.Height = 50;
            bd.Width = 200;
            bd.Text = "Bouton dynamique";
            bd.BackColor = Color.Red;
            bd.Margin = new Padding(30);
            bd.Click += new EventHandler(DynamiqueButtonClick);
            lstBp.Add(bd);
            flowLayoutPanel1.Controls.Add(bd);
        }


        private void DynamiqueButtonClick(object sender, EventArgs e)
        {
            Button b = sender as Button;
            for (int i = 0; i < lstBp.Count; i++)
            {
                if (b == lstBp[i])
                {
                    b.Text += $"*{i}";
                }
            }
        }

    }
}
