﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace _03_LibBdd
{
    public class ContactDao
    {
        static MySqlConnection cnx;
        public static string ChaineConnection { get; set; }

        public void SaveOrUpdate(Contact c, bool close = true)
        {
            if (c.Id == 0)
            {
                Create(c, close);
            }
            else
            {
                Update(c, close);
            }
        }

        public void Remove(Contact c, bool close = true)
        {
            Remove(c.Id, close);
        }

        public void Remove(long id, bool close = true)
        {
            string requete = "DELETE FROM contacts WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(requete, GetConnection());
            cmd.Parameters.AddWithValue("@id", id);
            cmd.ExecuteNonQuery();
            CloseConnection(close);
        }

        public Contact FindById(long id, bool close = true)
        {
            Contact c = null;
            string requete = "Select id,prenom,nom, date_naissance,email FROM contacts WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(requete, GetConnection());
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader r = cmd.ExecuteReader();
            if (r.Read())
            {
                c = new Contact(r.GetString(1), r.GetString(2), r.GetDateTime(3), r.GetString(4));
                c.Id = r.GetInt64(0);
            }
            r.Close();
            CloseConnection(close);
            return c;
        }

        public List<Contact> FindAll(bool close = true)
        {
            List<Contact> contacts = new List<Contact>();
            string requete = "Select id,prenom,nom, date_naissance,email FROM contacts";
            MySqlCommand cmd = new MySqlCommand(requete, GetConnection());
            MySqlDataReader r = cmd.ExecuteReader();
            while (r.Read())
            {
                Contact c = new Contact(r.GetString(1), r.GetString(2), r.GetDateTime(3), r.GetString(4));
                c.Id = r.GetInt64(0);
                contacts.Add(c);
            }
            r.Close();
            CloseConnection(close);
            return contacts;
        }


        private MySqlConnection GetConnection()
        {
            if (cnx == null)
            {
                cnx = new MySqlConnection(ChaineConnection);
                cnx.Open();
            }
            return cnx;
        }

        private void CloseConnection(bool close)
        {
            if (close && cnx != null)
            {
                cnx.Close();
                cnx.Dispose();
                cnx = null;
            }
        }

        // LastInsertedId => permet de récupérer l'id généré par la base de donnée (avec MySql)
        private void Create(Contact c, bool close)
        {
            string requete = "INSERT INTO contacts(prenom,nom,date_naissance,email) VALUES(@prenom,@nom,@date_naissance,@email)";
            MySqlCommand cmd = new MySqlCommand(requete, GetConnection());
            cmd.Parameters.AddWithValue("@prenom", c.Prenom);
            cmd.Parameters.AddWithValue("@nom", c.Nom);
            cmd.Parameters.AddWithValue("@date_naissance", c.DateNaissance);
            cmd.Parameters.AddWithValue("@email", c.Email);
            cmd.ExecuteNonQuery();
            c.Id = cmd.LastInsertedId;
            CloseConnection(close);
        }

        private void Update(Contact c, bool close)
        {
            string requete = "UPDATE contacts  SET prenom=@prenom ,nom=@nom, date_naissance=@date_naissance,email=@email WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(requete, GetConnection());
            cmd.Parameters.AddWithValue("@prenom", c.Prenom);
            cmd.Parameters.AddWithValue("@nom", c.Nom);
            cmd.Parameters.AddWithValue("@date_naissance", c.DateNaissance);
            cmd.Parameters.AddWithValue("@email", c.Email);
            cmd.Parameters.AddWithValue("@id", c.Id);
            cmd.ExecuteNonQuery();
            CloseConnection(close);
        }

    }
}
