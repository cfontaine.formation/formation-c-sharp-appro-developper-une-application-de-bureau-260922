﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace _03_LibBdd
{
    // La classe abstraite GenericDao est la classe mère de tous les dao
    // Elle permet de gérer la connection à la base de données et fournit les méthodes Create Read Update Delete 
    public abstract class GenericDao<T> where T : AbstractEntity
    {
        static MySqlConnection cnx;
        public static string ChaineConnection { get; set; }


        // Méthode pour persiter un objet ou le mettre à jour s'il existe dans la base de donnée
        // close à false permet de ne pas fermer la connection à la base de donnée
        public void SaveOrUpdate(T c, bool close = true)
        {
            if (c.Id == 0)
            {
                Create(GetConnection(), c);
            }
            else
            {
                Update(GetConnection(), c);
            }
            CloseConnection(close);
        }

        // Méthode pour supprimer un objet de la base de données
        public void Remove(T c, bool close = true)
        {
            Remove(GetConnection(), c.Id);
            CloseConnection(close);
        }


        public void Remove(long id, bool close = true)
        {
            Remove(GetConnection(), id);
            CloseConnection(close);
        }

        // Méthode pour récupérer un objet dans la base de donnée en fonction de son id
        public T FindById(long id, bool close = true)
        {
            T tmp = FindById(GetConnection(), id);
            CloseConnection(close);
            return tmp;
        }

        // Méthode pour récupérer tous les objets de la base de donnée
        public List<T> FindAll(bool close = true)
        {
            List<T> lst = FindAll(GetConnection());
            CloseConnection(close);
            return lst;
        }

        // Méthode qui permet d'obtenir la connection à la base de donnée
        protected MySqlConnection GetConnection()
        {
            if (cnx == null)
            {
                cnx = new MySqlConnection(ChaineConnection);
                cnx.Open();
            }
            return cnx;
        }

        // Méthode qui permet de fermer la connection à la base de donnée
        protected void CloseConnection(bool close)
        {
            if (close && cnx != null)
            {
                cnx.Close();
                cnx.Dispose();
                cnx = null;
            }
        }

        // Méthodes abstraites qui vont contenir les requêtes SQL dans les sous-classes
        protected abstract void Remove(MySqlConnection cnx, long id);
        protected abstract T FindById(MySqlConnection cnx, long id);
        protected abstract List<T> FindAll(MySqlConnection cnx);
        protected abstract void Create(MySqlConnection cnx, T c);
        protected abstract void Update(MySqlConnection cnx, T c);
    }
}
