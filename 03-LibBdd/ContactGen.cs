﻿using System;

namespace _03_LibBdd
{
    internal class ContactGen : AbstractEntity
    {
        public string Prenom { get; set; }

        public string Nom { get; set; }

        public DateTime DateNaissance { get; set; }

        public string Email { get; set; }

        public ContactGen(string prenom, string nom, DateTime dateNaissance, string email)
        {
            Prenom = prenom;
            Nom = nom;
            DateNaissance = dateNaissance;
            Email = email;
        }

        public override string ToString()
        {
            return $"{base.ToString()} {Prenom} {Nom} {DateNaissance} {Email}";
        }
    }
}
