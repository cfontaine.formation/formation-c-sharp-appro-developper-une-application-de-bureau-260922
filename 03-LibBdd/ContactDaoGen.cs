﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace _03_LibBdd
{
    internal class ContactDaoGen : GenericDao<ContactGen>
    {
        protected override void Create(MySqlConnection cnx, ContactGen c)
        {
            string requete = "INSERT INTO contacts(prenom,nom,date_naissance,email) VALUES(@prenom,@nom,@date_naissance,@email)";
            MySqlCommand cmd = new MySqlCommand(requete, cnx);
            cmd.Parameters.AddWithValue("@prenom", c.Prenom);
            cmd.Parameters.AddWithValue("@nom", c.Nom);
            cmd.Parameters.AddWithValue("@date_naissance", c.DateNaissance);
            cmd.Parameters.AddWithValue("@email", c.Email);
            cmd.ExecuteNonQuery();
            c.Id = cmd.LastInsertedId;
        }

        protected override List<ContactGen> FindAll(MySqlConnection cnx)
        {
            List<ContactGen> contacts = new List<ContactGen>();
            string requete = "Select id,prenom,nom, date_naissance,email FROM contacts";
            MySqlCommand cmd = new MySqlCommand(requete, cnx);
            MySqlDataReader r = cmd.ExecuteReader();
            while (r.Read())
            {
                ContactGen c = new ContactGen(r.GetString(1), r.GetString(2), r.GetDateTime(3), r.GetString(4));
                c.Id = r.GetInt64(0);
                contacts.Add(c);
            }
            r.Close();
            return contacts;
        }

        protected override ContactGen FindById(MySqlConnection cnx, long id)
        {
            ContactGen c = null;
            string requete = "Select id,prenom,nom, date_naissance,email FROM contacts WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(requete, cnx);
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader r = cmd.ExecuteReader();
            if (r.Read())
            {
                c = new ContactGen(r.GetString(1), r.GetString(2), r.GetDateTime(3), r.GetString(4));
                c.Id = r.GetInt64(0);
            }
            r.Close();
            return c;
        }

        protected override void Remove(MySqlConnection cnx, long id)
        {
            string requete = "DELETE FROM contacts WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(requete, cnx);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.ExecuteNonQuery();
        }

        protected override void Update(MySqlConnection cnx, ContactGen c)
        {
            string requete = "UPDATE contacts  SET prenom=@prenom ,nom=@nom, date_naissance=@date_naissance,email=@email WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(requete, cnx);
            cmd.Parameters.AddWithValue("@prenom", c.Prenom);
            cmd.Parameters.AddWithValue("@nom", c.Nom);
            cmd.Parameters.AddWithValue("@date_naissance", c.DateNaissance);
            cmd.Parameters.AddWithValue("@email", c.Email);
            cmd.Parameters.AddWithValue("@id", c.Id);
            cmd.ExecuteNonQuery();
        }

        // Dans le dao, on peut déclarer d'autre méthodes suplémentaires uniquement pour la classe Contact 
        public bool IsEmailExist(string email, bool close = true)   // test si un email existe dans la base de donnée
        {
            bool exist = false;
            string req = "SELECT email FROM contacts WHERE email=@email";
            using (MySqlCommand cmd = new MySqlCommand(req, GetConnection()))
            {
                cmd.Parameters.AddWithValue("@email", email);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        exist = true;
                    }
                }
            }
            CloseConnection(close);
            return exist;
        }

        public ContactGen FindByEmail(string email, bool close = true)    // Trouve un contact dans la bdd en fonction d'un email 
        {
            ContactGen contact = null;
            string req = "SELECT id,prenom,nom,date_naissance FROM contacts WHERE email=@email";
            using (MySqlCommand cmd = new MySqlCommand(req, GetConnection()))
            {
                cmd.Parameters.AddWithValue("@email", email);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        contact = new ContactGen(reader.GetString(1), reader.GetString(2), reader.GetDateTime(3), email);
                        contact.Id = reader.GetInt64(0);
                    }
                }
            }
            CloseConnection(close);
            return contact;
        }
    }
}
