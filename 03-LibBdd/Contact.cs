﻿using System;

namespace _03_LibBdd
{
    // La visibilité d'une classe par défaut est internal -> elle n'est isible en dehors du projet 03-LibBdd
    // pour que la classe Contact soit visible à l'exterieur, il faut que la visibilité soit public 
    public class Contact
    {
        public long Id { get; set; }
        public string Prenom { get; set; }

        public string Nom { get; set; }

        public DateTime DateNaissance { get; set; }

        public string Email { get; set; }

        public Contact(string prenom, string nom, DateTime dateNaissance, string email)
        {
            Prenom = prenom;
            Nom = nom;
            DateNaissance = dateNaissance;
            Email = email;
        }

        public override string ToString()
        {
            return $"{Id} {Prenom} {Nom} {DateNaissance} {Email}";
        }
    }
}
