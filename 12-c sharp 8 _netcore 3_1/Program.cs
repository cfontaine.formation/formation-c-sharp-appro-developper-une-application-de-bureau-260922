﻿using System;

namespace _12_Csharp_8_.netcore3._1
{
    internal class Program
    {
        // Quelques nouveautés de C# 8 
        // On ne peut utiliser le C# 8 que avec .net core 3.1 et .net 6.0 (.net 5.0 et .net core 3.0 ne sont plus supporté)
        // Avec .Net framework la version maximale utilisable est C# 7.2
        static void Main(string[] args)
        {
            #region switch avec les corps d'expression
            int numJour = int.Parse(Console.ReadLine());
            string jour = numJour switch
            {
                1 => "Lundi",
                2 => "Mardi",
                7 => "Dimanche",
                _ => "Un autre jour" // _ équivalant à 'default'
            };
            Console.WriteLine(jour);
            #endregion

            #region Indices et Ranges
            // Les indices (index) permettent d’accéder à un élément d'un tableau à partir de la fin du tableau, avec l'opérateur ^
            char[] v = new char[] { 'a', 'e', 'i', 'o', 'u' };
            Console.WriteLine(v[^1]);
            Console.WriteLine("_______________");

            // Les Ranges permettent d'accéder à une plage du tableau, en utilisant l'opérateur	..  ( le 2ème indice est exclue)
            char[] deuxPremier = v[..2];
            AfficherTab(deuxPremier);       // 'a', 'e'
            char[] troisDernier = v[2..];
            AfficherTab(troisDernier);      // 'i', 'o', 'u'
            char[] milieu = v[2..3];
            AfficherTab(milieu);            // 'i'
            char[] deuxDernier = v[^2..]; 
            AfficherTab(deuxDernier);       // 'o', 'u'
            #endregion

            //
            AfficherMultiple(2)

        }

        public static void AfficherTab<T>(T[] t ){
            foreach (T e in t)
            {
                Console.WriteLine(e);
            }
        }

        #region Méthode locale statique
        // En ajoutant static à une méthode locale, on l’empêche d’accéder aux variables locales et aux paramètres de la méthode englobante

        static void AfficherMultiple(int val)
        {
            // int a = 42;
            Console.WriteLine(Multiple(3));
            static int Multiple(int value)
            {
                // a et val ne sont plus accessible dans la méthode locale
                int res = value * 2;
                //int res = value * 2 * a;   
                //int res = value * 2 * val;
                return res;
            };
        }
        #endregion
    }
