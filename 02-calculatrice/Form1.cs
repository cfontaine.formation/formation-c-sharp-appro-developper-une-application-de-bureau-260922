﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02_calculatrice
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
          //  CultureInfo.CurrentCulture= new CultureInfo("en-us", false);
        }

        private void buttonPlus_Click(object sender, EventArgs e)
        {
            if(GetValeur(out double v1, out double v2))
            {
                textBoxRes.Text = $"{v1 + v2}";
            }
        }

    

        private void buttonMoins_Click(object sender, EventArgs e)
        {
            if (GetValeur(out double v1, out double v2))
            {
                textBoxRes.Text = $"{v1 - v2}";
            }
        }

        private void buttonMultiplier_Click(object sender, EventArgs e)
        {
            if (GetValeur(out double v1, out double v2))
            {
                textBoxRes.Text = $"{v1 * v2}";
            }
        }

        private void buttonDivision_Click(object sender, EventArgs e)
        {
            if (GetValeur(out double v1, out double v2))
            {
                textBoxRes.Text = $"{v1 / v2}";
            }
        }

        bool GetValeur(out double valeur1, out double valeur2)
        {
            valeur1 = 0;
            valeur2 = 0;
            try
            {
                return ConvValeur(textBoxValeur1, out valeur1) && ConvValeur(textBoxValeur2, out valeur2);
            }
            catch (FormatException e)
            {
                MessageBox.Show(e.Message, "Calculatrice", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxValeur1.Clear();
                textBoxValeur2.Clear();
                textBoxRes.Clear();
            }
            return false;
        }

        private bool ConvValeur(TextBox tb, out double valeur)
        {
            valeur = 0.0;
            if (tb.Text == "")
            {
                MessageBox.Show("Une des valeurs n'est pas saisie", "Calculatrice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {
                valeur = Convert.ToDouble(tb.Text);
                return true;
            }
        }


    }
}
