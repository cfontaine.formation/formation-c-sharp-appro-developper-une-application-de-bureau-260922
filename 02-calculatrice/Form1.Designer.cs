﻿namespace _02_calculatrice
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labelValeur1 = new System.Windows.Forms.Label();
            this.textBoxValeur1 = new System.Windows.Forms.TextBox();
            this.buttonPlus = new System.Windows.Forms.Button();
            this.buttonMoins = new System.Windows.Forms.Button();
            this.buttonMultiplier = new System.Windows.Forms.Button();
            this.buttonDivision = new System.Windows.Forms.Button();
            this.textBoxValeur2 = new System.Windows.Forms.TextBox();
            this.labelValeur2 = new System.Windows.Forms.Label();
            this.textBoxRes = new System.Windows.Forms.TextBox();
            this.labelResultat = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelValeur1
            // 
            this.labelValeur1.AutoSize = true;
            this.labelValeur1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValeur1.Location = new System.Drawing.Point(67, 70);
            this.labelValeur1.Name = "labelValeur1";
            this.labelValeur1.Size = new System.Drawing.Size(71, 20);
            this.labelValeur1.TabIndex = 0;
            this.labelValeur1.Text = "Valeur 1";
            // 
            // textBoxValeur1
            // 
            this.textBoxValeur1.Location = new System.Drawing.Point(177, 68);
            this.textBoxValeur1.Name = "textBoxValeur1";
            this.textBoxValeur1.Size = new System.Drawing.Size(100, 22);
            this.textBoxValeur1.TabIndex = 1;
            // 
            // buttonPlus
            // 
            this.buttonPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPlus.Location = new System.Drawing.Point(44, 147);
            this.buttonPlus.Name = "buttonPlus";
            this.buttonPlus.Size = new System.Drawing.Size(40, 38);
            this.buttonPlus.TabIndex = 2;
            this.buttonPlus.Text = "+";
            this.buttonPlus.UseVisualStyleBackColor = true;
            this.buttonPlus.Click += new System.EventHandler(this.buttonPlus_Click);
            // 
            // buttonMoins
            // 
            this.buttonMoins.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMoins.Location = new System.Drawing.Point(133, 147);
            this.buttonMoins.Name = "buttonMoins";
            this.buttonMoins.Size = new System.Drawing.Size(40, 38);
            this.buttonMoins.TabIndex = 3;
            this.buttonMoins.Text = "-";
            this.buttonMoins.UseVisualStyleBackColor = true;
            this.buttonMoins.Click += new System.EventHandler(this.buttonMoins_Click);
            // 
            // buttonMultiplier
            // 
            this.buttonMultiplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMultiplier.Location = new System.Drawing.Point(222, 147);
            this.buttonMultiplier.Name = "buttonMultiplier";
            this.buttonMultiplier.Size = new System.Drawing.Size(40, 38);
            this.buttonMultiplier.TabIndex = 4;
            this.buttonMultiplier.Text = "x";
            this.buttonMultiplier.UseVisualStyleBackColor = true;
            this.buttonMultiplier.Click += new System.EventHandler(this.buttonMultiplier_Click);
            // 
            // buttonDivision
            // 
            this.buttonDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDivision.Location = new System.Drawing.Point(311, 147);
            this.buttonDivision.Name = "buttonDivision";
            this.buttonDivision.Size = new System.Drawing.Size(40, 38);
            this.buttonDivision.TabIndex = 5;
            this.buttonDivision.Text = "/";
            this.buttonDivision.UseVisualStyleBackColor = true;
            this.buttonDivision.Click += new System.EventHandler(this.buttonDivision_Click);
            // 
            // textBoxValeur2
            // 
            this.textBoxValeur2.Location = new System.Drawing.Point(177, 240);
            this.textBoxValeur2.Name = "textBoxValeur2";
            this.textBoxValeur2.Size = new System.Drawing.Size(100, 22);
            this.textBoxValeur2.TabIndex = 7;
            // 
            // labelValeur2
            // 
            this.labelValeur2.AutoSize = true;
            this.labelValeur2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValeur2.Location = new System.Drawing.Point(67, 243);
            this.labelValeur2.Name = "labelValeur2";
            this.labelValeur2.Size = new System.Drawing.Size(71, 20);
            this.labelValeur2.TabIndex = 6;
            this.labelValeur2.Text = "Valeur 2";
            // 
            // textBoxRes
            // 
            this.textBoxRes.Location = new System.Drawing.Point(177, 317);
            this.textBoxRes.Name = "textBoxRes";
            this.textBoxRes.Size = new System.Drawing.Size(100, 22);
            this.textBoxRes.TabIndex = 9;
            // 
            // labelResultat
            // 
            this.labelResultat.AutoSize = true;
            this.labelResultat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultat.Location = new System.Drawing.Point(89, 320);
            this.labelResultat.Name = "labelResultat";
            this.labelResultat.Size = new System.Drawing.Size(19, 20);
            this.labelResultat.TabIndex = 8;
            this.labelResultat.Text = "=";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 383);
            this.Controls.Add(this.textBoxRes);
            this.Controls.Add(this.labelResultat);
            this.Controls.Add(this.textBoxValeur2);
            this.Controls.Add(this.labelValeur2);
            this.Controls.Add(this.buttonDivision);
            this.Controls.Add(this.buttonMultiplier);
            this.Controls.Add(this.buttonMoins);
            this.Controls.Add(this.buttonPlus);
            this.Controls.Add(this.textBoxValeur1);
            this.Controls.Add(this.labelValeur1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Calculatrice";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelValeur1;
        private System.Windows.Forms.TextBox textBoxValeur1;
        private System.Windows.Forms.Button buttonPlus;
        private System.Windows.Forms.Button buttonMoins;
        private System.Windows.Forms.Button buttonMultiplier;
        private System.Windows.Forms.Button buttonDivision;
        private System.Windows.Forms.TextBox textBoxValeur2;
        private System.Windows.Forms.Label labelValeur2;
        private System.Windows.Forms.TextBox textBoxRes;
        private System.Windows.Forms.Label labelResultat;
    }
}

