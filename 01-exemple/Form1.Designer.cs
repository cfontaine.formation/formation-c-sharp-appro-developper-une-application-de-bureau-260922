﻿namespace _01_exemple
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bpTest = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.labelValeur = new System.Windows.Forms.Label();
            this.textValeur = new System.Windows.Forms.TextBox();
            this.buttonValeur = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bpTest
            // 
            this.bpTest.Location = new System.Drawing.Point(54, 447);
            this.bpTest.Name = "bpTest";
            this.bpTest.Size = new System.Drawing.Size(143, 69);
            this.bpTest.TabIndex = 0;
            this.bpTest.Text = "Test";
            this.bpTest.UseVisualStyleBackColor = true;
            this.bpTest.Click += new System.EventHandler(this.bpTest_Click);
            this.bpTest.MouseEnter += new System.EventHandler(this.bpTest_MouseEnter);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(54, 304);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 69);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_2_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(290, 304);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(148, 69);
            this.button2.TabIndex = 2;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button1_2_Click);
            // 
            // labelValeur
            // 
            this.labelValeur.AutoSize = true;
            this.labelValeur.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValeur.Location = new System.Drawing.Point(50, 137);
            this.labelValeur.Name = "labelValeur";
            this.labelValeur.Size = new System.Drawing.Size(57, 20);
            this.labelValeur.TabIndex = 3;
            this.labelValeur.Text = "Valeur";
            // 
            // textValeur
            // 
            this.textValeur.BackColor = System.Drawing.SystemColors.Window;
            this.textValeur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textValeur.Location = new System.Drawing.Point(157, 137);
            this.textValeur.MaxLength = 10;
            this.textValeur.Name = "textValeur";
            this.textValeur.Size = new System.Drawing.Size(100, 22);
            this.textValeur.TabIndex = 4;
            // 
            // buttonValeur
            // 
            this.buttonValeur.Location = new System.Drawing.Point(290, 137);
            this.buttonValeur.Name = "buttonValeur";
            this.buttonValeur.Size = new System.Drawing.Size(41, 26);
            this.buttonValeur.TabIndex = 5;
            this.buttonValeur.Text = "Ok";
            this.buttonValeur.UseVisualStyleBackColor = true;
            this.buttonValeur.Click += new System.EventHandler(this.buttonValeur_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 621);
            this.Controls.Add(this.buttonValeur);
            this.Controls.Add(this.textValeur);
            this.Controls.Add(this.labelValeur);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bpTest);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(700, 450);
            this.Name = "Form1";
            this.Text = "Exemple";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button bpTest;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label labelValeur;
        private System.Windows.Forms.TextBox textValeur;
        private System.Windows.Forms.Button buttonValeur;
    }
}

