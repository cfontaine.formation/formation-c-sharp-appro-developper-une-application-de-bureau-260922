﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _01_exemple
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bpTest_Click(object sender, EventArgs e)
        {
            //bpTest.Text += "-";
            DialogResult res=MessageBox.Show("Vous avez cliqué sur le bouton", "Test", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No)
            {
                bpTest.Text = "Menteur";
            }
        }

        private void bpTest_MouseEnter(object sender, EventArgs e)
        {
            bpTest.Text += "+";
        }

        private void button1_2_Click(object sender, EventArgs e)
        {
            Button bp = sender as Button;
            MessageBox.Show("J'ai cliqué sur le bouton: "+ bp.Text);
        }

        private void buttonValeur_Click(object sender, EventArgs e)
        {
            
            if (textValeur.Text == "")
            {
                MessageBox.Show("Le champs est vide");
            }
            else
            {
                MessageBox.Show(textValeur.Text);
                textValeur.Clear();
            }
        }
    }
}
