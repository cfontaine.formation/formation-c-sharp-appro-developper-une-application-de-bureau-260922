﻿using System;
using System.Windows.Forms;

namespace _06_calculatriceV2
{
    public partial class Form1 : Form
    {

        private double resultat;

        private char operateur = ' ';

        private bool clear = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonNum_Click(object sender, EventArgs e)
        {
            if (clear)
            {
                textBoxResultat.Clear();
                clear = false;
            }
            Button bp = sender as Button;
            textBoxResultat.Text += bp.Text;
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            textBoxResultat.Clear();
        }

        private void buttonBackspace_Click(object sender, EventArgs e)
        {
            if (textBoxResultat.Text.Length > 0)
            {
                textBoxResultat.Text = textBoxResultat.Text.Substring(0, textBoxResultat.Text.Length - 1);
            }
        }

        private void buttonSigne_Click(object sender, EventArgs e)
        {
            if (textBoxResultat.Text != "")
            {
                if (textBoxResultat.Text.StartsWith("-"))
                {
                    textBoxResultat.Text = textBoxResultat.Text.Substring(1);
                }
                else
                {
                    textBoxResultat.Text = "-" + textBoxResultat.Text;
                }
            }
        }

        private void buttonVirgule_Click(object sender, EventArgs e)
        {
            if (textBoxResultat.Text != "" && !textBoxResultat.Text.Contains(","))
            {
                textBoxResultat.Text += ",";
            }
        }

        private void buttonOperateur_Click(object sender, EventArgs e)
        {
            Button bpOp = sender as Button;
            if (!clear && textBoxResultat.Text != "")
            {
                double val = Convert.ToDouble(textBoxResultat.Text);
                switch (operateur)
                {
                    case '+':
                        resultat += val;
                        break;
                    case '-':
                        resultat -= val;
                        break;
                    case 'x':
                        resultat *= val;
                        break;
                    case '/':
                        resultat /= val;
                        break;
                    case ' ':
                        resultat = val;
                        break;
                }

                textBoxResultat.Text = resultat.ToString();
                clear = true;
            }
            operateur = bpOp.Text[0];
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            textBoxResultat.Clear();
            resultat = 0.0;
            operateur = ' ';
        }
    }
}
